"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");
const GraphqlAdonis = use("ApolloServer");

const schema = require("../app/Graphql/schema");

Route.get("/", () => {
  return { greeting: "Hello world in JSON" };
});

Route.group(() => {
  Route.post("auth/login", "AuthController.login").middleware("guest");
  Route.get("auth/me", "AuthController.me").middleware("auth");

  Route.get("products", "ProductController.index");
  Route.post("products", "ProductController.store").middleware("auth");
  Route.get("products/:id", "ProductController.show");
  Route.put("products/:id", "ProductController.update").middleware("auth");
  Route.delete("products/:id", "ProductController.destroy").middleware("auth");
}).prefix("api");

Route.route(
  "/graphql",
  ({ request, auth, response }) => {
    return GraphqlAdonis.graphql(
      {
        schema,
        context: { auth },
      },
      request,
      response
    );
  },
  ["GET", "POST"]
);

Route.get("/graphiql", ({ request, response }) => {
  return GraphqlAdonis.graphiql({ endpointURL: "/graphql" }, request, response);
});
