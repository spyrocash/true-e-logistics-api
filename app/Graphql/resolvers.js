"use strict";

const Product = use("App/Models/Product");

const resolvers = {
  Query: {
    async profile(_, {}, { auth }) {
      return await auth.getUser();
    },
    async productList() {
      try {
        const products = await Product.all();
        return products.toJSON();
      } catch (error) {
        throw new Error(error.message);
      }
    },
    async productDetail(_, { id }) {
      try {
        const product = await Product.find(id);

        if (!product) {
          throw new Error("Product not found");
        }

        return product.toJSON();
      } catch (error) {
        throw new Error(error.message);
      }
    },
  },
  Mutation: {
    async login(_, { username, password }, { auth }) {
      try {
        const { token } = await auth.attempt(username, password);
        return token;
      } catch (error) {
        throw new Error(error.message);
      }
    },
    async productCreate(_, { name, price }, { auth }) {
      try {
        const user = await auth.getUser();

        return await Product.create({
          name,
          price,
          user_id: user.id,
        });
      } catch (error) {
        throw new Error(error.message);
      }
    },
    async productUpdate(_, { id, name, price }, { auth }) {
      try {
        const user = await auth.getUser();

        const product = await Product.find(id);

        if (!product) {
          throw new Error("Product not found");
        }

        product.name = name;
        product.price = price;
        product.user_id = user.id;

        return await product.save();
      } catch (error) {
        throw new Error(error.message);
      }
    },
    async productDelete(_, { id }, { auth }) {
      try {
        await auth.check();

        const product = await Product.find(id);

        if (!product) {
          throw new Error("Product not found");
        }

        return await product.delete();
      } catch (error) {
        throw new Error(error.message);
      }
    },
  },
  Product: {
    async user(productInJson) {
      const product = new Product();
      product.newUp(productInJson);

      const user = await product.user().fetch();
      return user.toJSON();
    },
  },
};

module.exports = resolvers;
