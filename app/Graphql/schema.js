"use strict";

const { makeExecutableSchema } = require("graphql-tools");

const resolvers = require("./resolvers");

const typeDefs = `
  type User {
    id: Int!
    username: String!
    email: String!
  }
  type Product {
    id: Int!
    name: String!
    price: Int!
    user: User!
  }
  type Query {
    profile: User
    productList: [Product]
    productDetail(id: Int!): Product
  }
  type Mutation {
    login (username: String!, password: String!): String
    productCreate (name: String!, price: Int!): Product
    productUpdate (id: Int!, name: String!, price: Int!): Product
    productDelete (id: Int!): Product
  }
`;

module.exports = makeExecutableSchema({ typeDefs, resolvers });
