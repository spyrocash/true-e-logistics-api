"use strict";

class AuthController {
  async login({ auth, request }) {
    try {
      const { username, password } = request.post();
      return {
        status: "success",
        error_message: null,
        data: await auth.attempt(username, password),
      };
    } catch (error) {
      return {
        status: "error",
        error_message: error.message,
        data: {},
      };
    }
  }

  async me({ auth }) {
    try {
      return {
        status: "success",
        error_message: null,
        data: await auth.getUser(),
      };
    } catch (error) {
      return {
        status: "error",
        error_message: "Missing or invalid jwt token",
        data: {},
      };
    }
  }
}

module.exports = AuthController;
