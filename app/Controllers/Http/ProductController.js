"use strict";

const Product = use("App/Models/Product");

class ProductController {
  async index({ request }) {
    try {
      const { page = 1, limit = 10 } = request.all();

      const products = await Product.query()
        .orderBy("id", "desc")
        .paginate(page, limit);

      return {
        status: "success",
        error_message: null,
        ...products.toJSON(),
      };
    } catch (error) {
      return {
        status: "error",
        error_message: error.message,
        data: {},
      };
    }
  }

  async store({ request }) {
    try {
      const productData = request.only(["name", "price"]);

      const product = await Product.create(productData);

      return {
        status: "success",
        error_message: null,
        data: product,
      };
    } catch (error) {
      return {
        status: "error",
        error_message: error.message,
        data: {},
      };
    }
  }

  async show({ params }) {
    try {
      const { id } = params;
      const product = await Product.find(id);

      return {
        status: "success",
        error_message: null,
        data: product,
      };
    } catch (error) {
      return {
        status: "error",
        error_message: error.message,
        data: {},
      };
    }
  }

  async update({ request, params }) {
    try {
      const { id } = params;
      const product = await Product.find(id);

      const productData = request.only(["name", "price"]);

      product.merge(productData);

      await product.save();

      return {
        status: "success",
        error_message: null,
        data: product,
      };
    } catch (error) {
      return {
        status: "error",
        error_message: error.message,
        data: {},
      };
    }
  }

  async destroy({ params }) {
    try {
      const { id } = params;
      const product = await Product.find(id);

      await product.delete();

      return {
        status: "success",
        error_message: null,
        data: product,
      };
    } catch (error) {
      return {
        status: "error",
        error_message: error.message,
        data: {},
      };
    }
  }
}

module.exports = ProductController;
